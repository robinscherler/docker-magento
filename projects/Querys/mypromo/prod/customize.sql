###DEFAULT QUERY TASCS
#removes all base urls except default when using dummy-sql with live data (invalid urls for test-environment)
DELETE FROM `core_config_data` WHERE `path` LIKE '%base_url%';

#set base urls for local env
REPLACE INTO core_config_data (scope, scope_id, path, value)
VALUES
    ('default', '0', 'web/unsecure/base_url', 'https://mypromo-prod.test/'),
    ('default', '0', 'web/secure/base_url', 'https://mypromo-prod.test/'),
    ('websites', '1', 'web/unsecure/base_url', 'https://mypromo-prod.test/'),
    ('websites', '1', 'web/secure/base_url', 'https://mypromo-prod.test/'),
    ('websites', '1', 'web/unsecure/base_link_url', 'https://mypromo-prod.test/'),
    ('websites', '1', 'web/secure/base_link_url', 'https://mypromo-prod.test/'),
    ('websites', '4', 'web/unsecure/base_url', 'https://mypromo-alternative-prod.test/'),
    ('websites', '4', 'web/secure/base_url', 'https://mypromo-alternative-prod.test/'),
    ('websites', '4', 'web/unsecure/base_link_url', 'https://mypromo-alternative-prod.test/'),
    ('websites', '4', 'web/secure/base_link_url', 'https://mypromo-alternative-prod.test/');

#change inventory stock sales channel code to fix error
UPDATE `inventory_stock_sales_channel` SET `code` = 'base' WHERE `code` = 'geiger';

#In store_website update website_codes
UPDATE `store_website` SET `code` = 'base' WHERE `website_id` = '1';

#Update cms-container urls
UPDATE cms_block SET content = REPLACE(content, 'https://admin-01.mypromo.com/', 'https://mypromo-prod.test/');
UPDATE cms_page SET content = REPLACE(content, 'https://admin-01.mypromo.com/', 'https://mypromo-prod.test/');


#disable js merge on local test environment
UPDATE `core_config_data` SET `value` = '0' WHERE `path` = 'dev/js/merge_files';
UPDATE `core_config_data` SET `value` = '0' WHERE `path` = 'dev/js/enable_js_bundling';
UPDATE `core_config_data` SET `value` = '0' WHERE `path` = 'dev/js/minify_files';

####GEIGER SPECIFIC
#change catalog search engine from amasty_elastic to mysql
UPDATE `core_config_data` SET `value` = 'MySQL' WHERE `path` = 'catalog/search/engine' AND `scope_id` = '0';

#change default image adapter from imagemagick to gd2
UPDATE `core_config_data` SET `value` = 'GD2' WHERE `path` = 'dev/image/default_adapter' AND `scope_id` = '0';

#disable sentry
UPDATE `core_config_data` SET `value` = '0' WHERE `path` = 'sentry/general/enabled' AND `scope_id` = '0';