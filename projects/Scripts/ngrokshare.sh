#!/bin/bash

ngrok http http://mgo-prod.loc/ --host-header mgo-prod.loc

#setzen der url
shareurl="$(curl --silent --show-error http://127.0.0.1:4040/api/tunnels | sed -nE 's/.*public_url":"https:..([^"]*).*/\1/p')"
dockergento magento config:set web/unsecure/base_url "http://$shareurl/"
dockergento magento config:set web/secure/base_url "http://$shareurl/"

#alternatives automatisches setzen der url durch extension
# ./composer.phar require shkoliar/magento-ngrok