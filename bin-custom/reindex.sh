#!/bin/bash
set -o errexit

echo "Re-indexing ..."
bin/clinotty bin/magento indexer:reindex

echo "Clearing the cache to apply updates..."
bin/clinotty bin/magento cache:flush