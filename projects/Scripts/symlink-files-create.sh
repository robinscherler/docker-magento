#!/bin/sh
set -e

#import configs
. ./../Config/config.sh

ln -s $SYNC/* $PROJECT_PATH
ln -s $CONFIG/.env $PROJECT_PATH

echo "created symlinks:"
find $PROJECT_PATH -maxdepth 1 -type l
