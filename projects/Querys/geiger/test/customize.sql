###DEFAULT QUERY TASCS
#removes all base urls except default when using dummy-sql with live data (invalid urls for test-environment)
DELETE FROM `core_config_data` WHERE `path` LIKE '%base_url%';

#set base urls for local env
REPLACE INTO core_config_data (scope, scope_id, path, value)
VALUES
    ('default', '0', 'web/unsecure/base_url', 'http://geiger-test.loc/'),
    ('default', '0', 'web/secure/base_url', 'http://geiger-test.loc/'),
    ('websites', '1', 'web/unsecure/base_url', 'http://geiger-test.loc/'),
    ('websites', '1', 'web/secure/base_url', 'http://geiger-test.loc/');

#change inventory stock sales channel code to fix error
UPDATE `inventory_stock_sales_channel` SET `code` = 'base' WHERE `code` = 'geiger';

#In store_website update website_codes
UPDATE `store_website` SET `code` = 'base' WHERE `website_id` = '1';

#Update cms-container urls
UPDATE cms_block SET content = REPLACE(content, 'https://geiger.stage-00.aws.rissc.net/', 'http://geiger-test.loc/');
UPDATE cms_page SET content = REPLACE(content, 'https://geiger.stage-00.aws.rissc.net/', 'http://geiger-test.loc/');


#disable js merge on local test environment
UPDATE `core_config_data` SET `value` = '0' WHERE `path` = 'dev/js/merge_files';
UPDATE `core_config_data` SET `value` = '0' WHERE `path` = 'dev/js/enable_js_bundling';
UPDATE `core_config_data` SET `value` = '0' WHERE `path` = 'dev/js/minify_files';

####GEIGER SPECIFIC
#change catalog search engine from amasty_elastic to mysql
UPDATE `core_config_data` SET `value` = 'MySQL' WHERE `path` = 'catalog/search/engine' AND `scope_id` = '0';

#change default image adapter from imagemagick to gd2
UPDATE `core_config_data` SET `value` = 'GD2' WHERE `path` = 'dev/image/default_adapter' AND `scope_id` = '0';

#disable sentry
UPDATE `core_config_data` SET `value` = '0' WHERE `path` = 'sentry/general/enabled' AND `scope_id` = '0';

#configure elastic
REPLACE INTO `core_config_data` (`scope`, `scope_id`, `path`, `value`)
VALUES
('default', 0, 'catalog/search/elasticsearch_server_hostname', 'elasticsearch'),
('default', 0, 'catalog/search/elasticsearch_server_port', '9200'),
('default', 0, 'catalog/search/elasticsearch_index_prefix', 'magento'),
('default', 0, 'catalog/search/elasticsearch_enable_auth', '1'),
('default', 0, 'catalog/search/elasticsearch_username', 'magento'),
('default', 0, 'catalog/search/elasticsearch_password', 'magento'),
('default', 0, 'catalog/search/elasticsearch7_server_timeout', '15'),
('default', 0, 'catalog/search/engine', 'elasticsearch7'),
('default', 0, 'catalog/search/elasticsearch7_password', 'magento'),
('default', 0, 'catalog/search/elasticsearch7_username', 'magento'),
('default', 0, 'catalog/search/elasticsearch7_enable_auth', '1'),
('default', 0, 'catalog/search/elasticsearch7_index_prefix', 'magento'),
('default', 0, 'catalog/search/elasticsearch7_server_port', '9200'),
('default', 0, 'catalog/search/elasticsearch7_server_hostname', 'elasticsearch');