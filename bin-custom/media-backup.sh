#!/bin/bash
bin/clinotty test -d pub/media

echo "media found";
RandomHash=$(cat /dev/urandom | env LC_CTYPE=C tr -cd 'a-f0-9' | head -c 32)
bin/clinotty mv pub/media pub/media_$RandomHash
echo "media moved to pub/media_$RandomHash";
