#!/bin/bash
set -o errexit

#enable all modules except marked modules to disable
bin/clinotty bin/magento module:enable --all

#disable all modules marked to disable
DISABLE_FILE=src/deploy_opsworks_disable_modules.csv
if test -f "$DISABLE_FILE"; then
    echo "$DISABLE_FILE exists."
    DISABLE_MODULES=$(cat src/deploy_opsworks_disable_modules.csv)
    if ! test -z "$DISABLE_MODULES"; then
        echo "$DISABLE_FILE is not empty."
        bin/clinotty bin/magento module:disable -f $DISABLE_MODULES
    fi
fi