#credentials db user

#root user
- name:
  - root
- pw:
  - magento

#normal user
- name:
  - magento
- pw:
  - magento

#how to handle product safe issue on mgo-pp stage dump:
- go to db-table "catalog_product_option" and delete definer trigger under "relations" *maybe only with root user possible