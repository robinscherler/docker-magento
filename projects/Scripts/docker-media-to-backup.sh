#!/bin/bash
set -e

if [ -d ./pub/media ]; then
    echo "media found";
    RandomHash=$(cat /dev/urandom | env LC_CTYPE=C tr -cd 'a-f0-9' | head -c 32)
    mv ./pub/media pub/media_$RandomHash
    echo "media moved to pub/media_$RandomHash";
else
    echo 'false';
fi
