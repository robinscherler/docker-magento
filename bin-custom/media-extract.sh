#!/bin/bash
source .env

#bin-custom/media-backup.sh

if [[ -f sources/$PRJ/$PRJ-$ENV/media.tar.gz ]]; then
    printf "[DOCKER_DEPLOY] media backup found -> extract\n"
    bin/clinotty tar xf sources/$PRJ/$PRJ-$ENV/media.tar.gz -C pub/ -vvv
fi

##verify link exists in compose.dev.yaml - ./src/external/Magento2/magento2-sample-data:/var/www/html/external/Magento2/magento2-sample-data:delegated
##and compose.dev.yaml is loaded * after copytocontainer
if [[ -d src/external/Magento2/magento2-sample-data ]]; then
    printf "[DOCKER_DEPLOY] start sample-files-setup\n"
    bin/clinotty php -f external/Magento2/magento2-sample-data/dev/tools/build-sample-data.php -- --ce-source="./"
fi