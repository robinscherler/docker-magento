#!/usr/bin/env bash
set -e

#import configs
. ./../Config/config.sh

DEPLOYMENT_START_DATE=$(date +%D)
DEPLOYMENT_START_TIME=$(date +%T)
TIMER_START=$(date +%s)
printf "_____________________________________________________________\n"
printf "[DOCKER_DEPLOY] deployment start: %s" $DEPLOYMENT_START_DATE
printf " %s\n" $DEPLOYMENT_START_TIME
printf "\n"

printf "_____________________________________________________________\n"
printf "[DOCKER_DEPLOY] list start configuration to check:\n"
printf "(please update config.sh if something is missing by checking config.sample.sh)\n"
printf "PROJECT_NAME is: %s\n" $PROJECT_NAME
printf "PROJECT_ENV is: %s\n" $PROJECT_ENV
printf "PROJECT is: %s\n" $PROJECT
printf "PROJECT_PATH is: %s\n" $PROJECT_PATH
printf "DOM is: %s\n" $DOM
printf "URL is: %s\n" $URL
printf "DOCKERGENTO is: %s\n" $DOCKERGENTO
printf "SOURCES is: %s\n" $SOURCES
printf "CONFIG is: %s\n" $CONFIG
printf "SYNC is: %s\n" $SYNC
printf "DB is: %s\n" $DB
printf "ACTION is: %s\n" $ACTION
printf "DB_HOST is: %s\n" $DB_HOST
printf "DB_USER is: %s\n" $DB_USER
printf "DB_PASSWORD is: %s\n" $DB_PASSWORD
printf "LANGUAGE is: %s\n" $LANGUAGE
printf "TIMEZONE is: %s\n" $TIMEZONE
printf "CURRENCY is: %s\n" $CURRENCY
printf "BACKEND_FRONTNAME is: %s\n" $BACKEND_FRONTNAME
printf "ADMIN_USER is: %s\n" $ADMIN_USER
printf "ADMIN_PASSWORD is: %s\n" $ADMIN_PASSWORD
printf "ADMIN_EMAIL is: %s\n" $ADMIN_EMAIL
printf "ADMIN_FIRSTNAME is: %s\n" $ADMIN_FIRSTNAME
printf "ADMIN_LASTNAME is: %s\n" $ADMIN_LASTNAME
printf "IMPORT_PATH is: %s\n" $IMPORT_PATH
printf "HOST_IMPORT_PATH is: %s\n" $HOST_IMPORT_PATH
printf "ISSUES_PATH is: %s\n" $ISSUES_PATH
printf "\n"

# cd into project folder to ececute commands
cd $PROJECT_PATH

if [ -z "$1" ]; then
    printf "_____________________________________________________________\n"
    printf "[DOCKER_DEPLOY] first param *Action* missing (update | install)\n"
    printf "[DOCKER_DEPLOY] default setting *update* is set\n"
    ACTION="update"
else
    ACTION=$1
fi

# Runs occurs on composer install command
# https://stackoverflow.com/questions/22390001/runtimeexception-vendor-does-not-exist-and-could-not-be-created
# RuntimeException vendor does not exist and could not be created
fix_permissions_issue_runtime_exception() {
    dockergento exec sudo chown -R app /var/www/html
}

run_composer_reload() {
    #outcomment to remove local files
    #rm -rf ./vendor ./generated ./var

    #outcomment to remove docker files
    printf "_____________________________________________________________\n"
    printf "[DOCKER_DEPLOY] remove old composer files\n"
    dockergento exec rm -rf ./vendor ./generated ./var/cache ./var/page_cache ./var/view_processed
    printf "\n"

    fix_permissions_issue_runtime_exception

    printf "_____________________________________________________________\n"
    printf "[DOCKER_DEPLOY] run composer (-vvv for complete log in console, also for update command recommended)\n"
    dockergento exec ./composer.phar install --no-dev --no-interaction --optimize-autoloader

    #dockergento exec cp $ISSUES_PATH/vendor/$SOURCE vendor/$DESTINATION

    if [ "$PROJECT_NAME" == "mgo" ]; then
        dockergento exec cp $ISSUES_PATH/vendor/amasty/groupcat/etc/search_request.xml vendor/amasty/groupcat/etc/search_request.xml
    fi
}

start_dockergento_tascs() {

    ###DO IT MANUALLY IF NO DB IS INSTALLED (~/Docker/DB/Data/$PROJECT) AND WAIT FOR COMPLETE DUMP-IMPORTS###
    if [ "$ACTION" == "initial" ]; then
        printf "_____________________________________________________________\n"
        dockergento start
    fi

    ###RUN SPECIFIED METHOD TO DEPLOY (NECESSARY TO REMOVE IMPORT_DUMPS (DOCKER-COMPOSER.YML - VOLUMES WITH DOCKER-ENTRYPOINT-INITDB.D)
    if [ "$ACTION" == "reset" ] || [ "$ACTION" == "test-reset" ]; then
        ###RESTART REMOVING AUTO-GENERATED PROJECT FILES###
        printf "_____________________________________________________________\n"
        printf "[DOCKER_DEPLOY] remove old docker-compose project\n"
        dockergento docker-compose down
        printf "\n"

        ###CLEAN RESTART OF CURRENT VOLUMES INCL CORRECT REMOUNT WITHOUT DELETING OTHER VOLUMES###
        printf "[DOCKER_DEPLOY] remove old volume\n"
        docker volume rm $(docker volume ls -q -f 'name='$PROJECT) #for example for $PROJECT
        printf "\n"

        ###DB DUMP IMPORT.SQL IN DOCKER-COMPOSE.YML UNDER DB VOLUMES###
        printf "[DOCKER_DEPLOY] run dockergento start\n"
        dockergento start
        printf "\n"

        run_composer_reload
        printf "\n"

        ###SEARCH REQUIRED CONFIG-FILES TO COPY AND USE THEM OR RUN INSTALLATION AGAINST NEW VOLUME
        printf "_____________________________________________________________\n"
        printf "[DOCKER_DEPLOY] search config files\n"
        if [ -e $HOST_IMPORT_PATH/env.php ] && [ -e $HOST_IMPORT_PATH/config.php ]; then
            printf "[DOCKER_DEPLOY] config files found\n"
            printf "[DOCKER_DEPLOY] copy env.php to magento\n"
            printf "[DOCKER_DEPLOY] copy config.php to magento\n"
            dockergento exec cp $IMPORT_PATH/env.php ./app/etc/env.php
            dockergento exec cp $IMPORT_PATH/config.php ./app/etc/config.php
            printf "\n"

            if [ "$ACTION" != "test-reset" ]; then
                if [ -f $HOST_IMPORT_PATH/media.tar.gz ]; then
                    printf "_____________________________________________________________\n"
                    dockergento exec ./app/code/Rissc/Dockergento/Sources/Scripts/docker-media-to-backup.sh
                    printf "[DOCKER_DEPLOY] extracting media-files in a few minutes (-vvv for complete log in console)\n"
                    dockergento exec tar xf $IMPORT_PATH/media.tar.gz -C pub
                    printf "\n"
                fi
            fi
        else
            printf "_____________________________________________________________\n"
            printf "[DOCKER_DEPLOY] config files not found\n"
            printf "[DOCKER_DEPLOY] start magento installation \n"
            #bin/magento module:status | grep -v Magento | grep -v "List of" | grep -v None | grep -v -e '^$' | sort | xargs bin/magento module:disable -f
            dockergento magento setup:install --base-url=$URL --db-host=$DB_HOST --db-name=$DB --db-user=$DB_USER --db-password=$DB_PASSWORD --language=$LANGUAGE --timezone=$TIMEZONE --currency=$CURRENCY --backend-frontname=$BACKEND_FRONTNAME --admin-firstname=$ADMIN_FIRSTNAME --admin-lastname=$ADMIN_LASTNAME --admin-email=$ADMIN_EMAIL --admin-user=$ADMIN_USER --admin-password=$ADMIN_PASSWORD
            printf "[DOCKER_DEPLOY] copy env.php to source-folder\n"
            printf "[DOCKER_DEPLOY] copy config.php to source-folder\n"
            dockergento exec cp ./app/etc/env.php $IMPORT_PATH/env.php
            dockergento exec cp ./app/etc/config.php $IMPORT_PATH/config.php
            printf "\n"

            if [ "$ACTION" != "test-reset" ]; then
                if [ -d ./pub./media ]; then
                    RandomHash=$(cat /dev/urandom | env LC_CTYPE=C tr -cd 'a-f0-9' | head -c 32)
                    mv ./pub/media pub/media_$RandomHash
                fi

                if [ -f $HOST_IMPORT_PATH/media.tar.gz ]; then
                    printf "_____________________________________________________________\n"
                    dockergento exec ./app/code/Rissc/Dockergento/Sources/Scripts/docker-media-to-backup.sh
                    printf "[DOCKER_DEPLOY] extracting media-files in a few minutes (-vvv for complete log in console)\n"
                    dockergento exec tar xf $IMPORT_PATH/media.tar.gz -C pub
                    printf "\n"
                fi
            fi

            if [[ -d external/magento2-sample-data ]]; then
                printf "[DOCKER_DEPLOY] start sample-files-setup\n"
                dockergento exec php -f external/magento2-sample-data/dev/tools/build-sample-data.php -- --ce-source="./"
            fi
        fi
    ###FOR LARGE DB-IMPORTS WHICH NEED MORE TIME THEN THE MEDIA IMPORT
    elif [ "$ACTION" == "db-reset" ]; then
        ###RESTART REMOVING AUTO-GENERATED PROJECT FILES###
        printf "_____________________________________________________________\n"
        printf "[DOCKER_DEPLOY] remove old docker-compose project\n"
        dockergento docker-compose down

        ###CLEAN RESTART OF CURRENT VOLUMES INCL CORRECT REMOUNT WITHOUT DELETING OTHER VOLUMES###
        printf "[DOCKER_DEPLOY] remove old volume\n"
        docker volume rm $(docker volume ls -q -f 'name='$PROJECT) #for example for $PROJECT

        ###DB DUMP IMPORT.SQL IN DOCKER-COMPOSE.YML UNDER DB VOLUMES###
        printf "[DOCKER_DEPLOY] run dockergento start\n"
        dockergento start
        printf "\n"
    ###COMPOSER UPDATE WITHOUT REMOVING DOCKER VOLUMES
    elif [ "$ACTION" == "update" ]; then
        run_composer_reload
        printf "\n"
    ###TO CREATE ONLY THE ADMIN USER
    elif [ "$ACTION" == "create-admin" ]; then
        printf "_____________________________________________________________\n"
        printf "[DOCKER_DEPLOY] create admin user\n"
        dockergento magento admin:user:create --admin-user=$ADMIN_USER --admin-password=$ADMIN_PASSWORD --admin-email=$ADMIN_EMAIL --admin-firstname=$ADMIN_FIRSTNAME --admin-lastname=$ADMIN_LASTNAME
        printf "\n"
    fi

    ###ADDITIONAL LOGIC FOR ALL STEPS EXCEPTING DB-RESET
    if [ "$ACTION" != "db-reset" ] && [ "$ACTION" != "create-admin" ]; then
        ###TO SYNC / UPLOAD IF NEEDED ON HOST###
        #dockergento mirror-container app/etc/env.php app/etc/config.php

        ###DO IT ONLY MANUALLY AND WAIT FOR PHPSTORM (NECESSARY ON HOST FOR DEBUGGING- & ANALYSIS-PURPOSES ON HOST)###
        #dockergento mirror-container vendor

        ###OUTCOMMENT FOR CURRENT MEDIA BACKUP OF HOST###
        #RandomHash=$(cat /dev/urandom | env LC_CTYPE=C tr -cd 'a-f0-9' | head -c 32)
        #mv pub/media pub/media_$RandomHash

        ###FOR EXTRACTION WITH MEDIA.TAR-GZ FILE ON HOST###
        #tar xf $IMPORT_PATH/media.tar.gz -C pub

        printf "_____________________________________________________________\n"
        printf "[DOCKER_DEPLOY] prepare cache\n"
        dockergento magento cache:enable
        dockergento magento cache:disable block_html full_page
        printf "\n"

        printf "_____________________________________________________________\n"
        printf "[DOCKER_DEPLOY] prepare modules\n"
        dockergento magento module:enable --all
        if [ -f $PROJECT_PATH/deploy_opsworks_disable_modules.csv ]; then
            dockergento magento module:disable-by-csv -f -o
        fi
        printf "\n"

        printf "_____________________________________________________________\n"
        printf "[DOCKER_DEPLOY] set deploy-mode\n"
        if [ "$ACTION" == "production" ]; then
            dockergento magento deploy:mode:set production --skip-compilation
        else
            dockergento magento deploy:mode:set developer --skip-compilation
        fi
        printf "\n"

        printf "_____________________________________________________________\n"
        printf "[DOCKER_DEPLOY] setup:upgrade\n"
        dockergento magento setup:upgrade
        printf "\n"

        if [ "$ACTION" == "production" ] || [ "$ACTION" == "test" ] || [ "$ACTION" == "test-reset" ]; then
            printf "_____________________________________________________________\n"
            printf "[DOCKER_DEPLOY] setup:di:compile\n"
            dockergento magento setup:di:compile
            printf "\n"
        fi

        if [ "$ACTION" == "production" ]; then
            dockergento magento setup:static-content:deploy de_DE en_US -f
        fi

        if [ "$ACTION" == "reset" ]; then
            printf "_____________________________________________________________\n"
            printf "[DOCKER_DEPLOY] run reindex\n"
            dockergento magento indexer:reindex
            printf "\n"
        fi

        printf "_____________________________________________________________\n"
        printf "[DOCKER_DEPLOY] reset Cache\n"
        dockergento magento cache:clean
        dockergento magento cache:flush
        printf "\n"

        if [ -f .idea/misc.xml ]; then
            printf "_____________________________________________________________\n"
            printf "[DOCKER_DEPLOY] generate urn for phpstorm under .idea/misc.xml (check .yml for symlink to container) \n"
            dockergento magento dev:urn-catalog:generate .idea/misc.xml
            printf "\n"
        fi
    fi
}

start_dockergento_tascs $1
DEPLOYMENT_END_DATE=$(date "+%D")
DEPLOYMENT_END_TIME=$(date "+%T")

printf "_____________________________________________________________\n"
printf "[DOCKER_DEPLOY] deployment start: %s" $DEPLOYMENT_START_DATE
printf " %s\n" $DEPLOYMENT_START_TIME
printf "[DOCKER_DEPLOY] deployment end: %s" $DEPLOYMENT_END_DATE
printf " %s\n" $DEPLOYMENT_END_TIME

TIMER_END=$(date +%s)
SECS=$(($TIMER_END - $TIMER_START))
printf "[DOCKER_DEPLOY] deployment duration: %dh:%dm:%ds\n" $((SECS % 86400 / 3600)) $((SECS % 3600 / 60)) $((SECS % 60))
printf "\n"
