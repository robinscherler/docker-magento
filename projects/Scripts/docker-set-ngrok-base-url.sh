#!/bin/sh
set -e

#import configs
. ./../Config/config.sh

# cd into project folder to execute commands
cd $PROJECT_PATH

DOMAIN=$(curl --silent --show-error http://127.0.0.1:4040/api/tunnels | sed -nE 's/.*public_url":"https:..([^"]*).*/\1/p')
URL="http://"$DOMAIN"/"
dockergento exec bin/magento setup:store-config:set --base-url=$URL