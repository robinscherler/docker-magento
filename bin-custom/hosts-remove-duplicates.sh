#!/bin/bash
set -o errexit

#this line is for backup without removing by pattern ".ngrok.io" *
sudo cp /etc/hosts /etc/hosts.bak

#remove duplicate lines from /etc/hosts (temp file)
HOSTS=$(cat -n /etc/hosts | sort -uk2 | sort -n | cut -f2-)
touch tmp-hosts.txt
echo "$HOSTS" > tmp-hosts.txt
sudo cp tmp-hosts.txt /etc/hosts
rm tmp-hosts.txt