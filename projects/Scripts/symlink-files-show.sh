#!/bin/sh
set -e

#import configs
. ./../Config/config.sh

echo "created symlinks:"
find $PROJECT_PATH -maxdepth 1 -type l
