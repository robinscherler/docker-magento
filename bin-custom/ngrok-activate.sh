#!/bin/bash
set -o errexit

ACTION=$1
STORE_CODE=$2

#help
#https://black.bird.eu/en/blog/how-to-use-ngrok-with-magento.html

#ngrok init command
#ngrok http https://mgo-dev.test/ -bind-tls=true

#ngrok example result:
#Forwarding   https://fb27-2003-e1-8721-3f7-c4e2-7b08-969d-9a69.ngrok.io -> http://localhost:80


#Set bin/magento as current folder position
CURRENT_NGROK="$(curl --silent --show-error http://localhost:4040/api/tunnels | sed -nE 's/.*public_url":"https:..([^"]*).*/\1/p')"
CURRENT_NGROK_URL="https://$CURRENT_NGROK/"

if [[ $ACTION == 'enable' ]]; then

    echo 'start ngrok'

    #lines to add url into /etc/hosts
    #HOSTS_FILE_PATH="/etc/hosts"
    #FILE_CONTENT=$( cat "${HOSTS_FILE_PATH}" )
    #if ! [[ " $FILE_CONTENT " =~ $CURRENT_NGROK ]]; then
    #    echo "127.0.0.1 ::1 $CURRENT_NGROK" | sudo tee -a /etc/hosts
    #    echo $CURRENT_NGROK" saved into "$HOSTS_FILE_PATH
    #fi

    CURRENT_BASE_URL=$(bin/clinotty bin/magento config:show --scope=default web/secure/base_url | sed 's/[[:space:]]//g')

    TMP_STRING=$CURRENT_BASE_URL
    TMP_LENGTH=${#TMP_STRING}
    TMP_LAST_CHAR=${TMP_STRING:TMP_LENGTH-1:1}

    if [[ $TMP_LAST_CHAR == "/" ]]; then
        TMP_STRING=${TMP_STRING:0:TMP_LENGTH-1};
        CURRENT_BASE_URL=$TMP_STRING"/";
    fi

    if [[ $CURRENT_BASE_URL != *"ngrok.io"* ]]; then
        echo "Safe current base_url as backup to ngrok/general/base_url_backup - "$CURRENT_BASE_URL
        bin/clinotty bin/magento config:set ngrok/general/base_url_backup "$CURRENT_BASE_URL"
        echo $CURRENT_NGROK_URL

        echo "Adding ngrok url as default base_url"
        bin/clinotty bin/magento config:set --scope=default web/unsecure/base_url "$CURRENT_NGROK_URL"
        bin/clinotty bin/magento config:set --scope=default web/secure/base_url "$CURRENT_NGROK_URL"

        if [[ $STORE_CODE ]]; then
            bin/clinotty bin/magento config:set --scope=websites --scope-code=$STORE_CODE web/unsecure/base_url "$CURRENT_NGROK_URL"
            bin/clinotty bin/magento config:set --scope=websites --scope-code=$STORE_CODE web/secure/base_url "$CURRENT_NGROK_URL"
        fi
        bin/setup-ssl "$CURRENT_NGROK"
    else
        if [[ "$CURRENT_BASE_URL" != *"$CURRENT_NGROK_URL"* ]]; then
            echo "New ngrok url found! Adding new ngrok url as default base_url"
            echo $CURRENT_NGROK_URL
            bin/clinotty bin/magento config:set --scope=default web/unsecure/base_url "$CURRENT_NGROK_URL"
            bin/clinotty bin/magento config:set --scope=default web/secure/base_url "$CURRENT_NGROK_URL"
            if [[ $STORE_CODE ]]; then
                bin/clinotty bin/magento config:set --scope=websites --scope-code=$STORE_CODE web/unsecure/base_url "$CURRENT_NGROK_URL"
                bin/clinotty bin/magento config:set --scope=websites --scope-code=$STORE_CODE web/secure/base_url "$CURRENT_NGROK_URL"
            fi
            bin/setup-ssl "$CURRENT_NGROK"
        else
            echo "Current ngrok url already saved as base_url"
            if [[ $STORE_CODE ]]; then
                CURRENT_STORE_URL=$(bin/clinotty bin/magento config:show --scope=websites --scope-code=$STORE_CODE web/secure/base_url | sed 's/[[:space:]]//g')
                if [[ "$CURRENT_STORE_URL" != *"$CURRENT_NGROK_URL"* ]]; then
                        bin/clinotty bin/magento config:set --scope=websites --scope-code=$STORE_CODE web/unsecure/base_url "$CURRENT_NGROK_URL"
                        bin/clinotty bin/magento config:set --scope=websites --scope-code=$STORE_CODE web/secure/base_url "$CURRENT_NGROK_URL"
                fi
            fi
            bin/setup-ssl "$CURRENT_NGROK"
        fi
    fi

elif [[ $ACTION == 'disable' ]]; then

    echo 'stop ngrok'

    CURRENT_BASE_URL=$(bin/clinotty bin/magento config:show --scope=default web/secure/base_url | sed 's/[[:space:]]//g')
    CURRENT_BACKUP_URL=$(bin/clinotty bin/magento config:show ngrok/general/base_url_backup | sed 's/[[:space:]]//g')

    if [[ "$CURRENT_BACKUP_URL" != *"ngrok.io"* ]]; then
        if [[ $CURRENT_BASE_URL == *"ngrok.io"* ]]; then
            bin/clinotty bin/magento config:set --scope=default web/unsecure/base_url "$CURRENT_BACKUP_URL"
            bin/clinotty bin/magento config:set --scope=default web/secure/base_url "$CURRENT_BACKUP_URL"
        fi
        if [[ $STORE_CODE ]]; then
            CURRENT_STORE_URL=$(bin/clinotty bin/magento config:show --scope=websites --scope-code=$STORE_CODE web/secure/base_url | sed 's/[[:space:]]//g')
            if [[ $CURRENT_STORE_URL == *"ngrok.io"* ]]; then
                bin/clinotty bin/magento config:set --scope=websites --scope-code=$STORE_CODE web/unsecure/base_url "$CURRENT_BACKUP_URL"
                bin/clinotty bin/magento config:set --scope=websites --scope-code=$STORE_CODE web/secure/base_url "$CURRENT_BACKUP_URL"
            fi
        fi
        bin/setup-ssl "$CURRENT_NGROK"
    fi

fi

bin/clinotty bin/magento c:f