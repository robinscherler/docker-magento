-- Declare the variable to be used.
-- Initialize the variable.
# SET @oldBaseUrl = 'https://mgo.mgo-mediafactory.de';
SET @oldBaseUrl = 'https://magento-stage.mgo-mediafactory.de';
SET @newBaseUrl = 'http://mgo-prod.test';
SET @oldAlternativeUrl = 'https://dennree.mgo-mediafactory.de';
# SET @newAlternativeUrl = 'https://dennree.mgo-mediafactory.test';
SET @newAlternativeUrl = 'http://mgo-prod.test';
SET @newBaseHomeUrl = concat(@newBaseUrl, '/home');

#Update cms-container urls
UPDATE cms_block SET content = REPLACE(content, @oldBaseUrl, @newBaseUrl);
UPDATE cms_page SET content = REPLACE(content, @oldBaseUrl, @newBaseUrl);
UPDATE cms_block SET content = REPLACE(content, @oldAlternativeUrl, @newAlternativeUrl);
UPDATE cms_page SET content = REPLACE(content, @oldAlternativeUrl, @newAlternativeUrl);
UPDATE magestore_megamenu_megamenu SET link = REPLACE(link, @oldBaseUrl, @newBaseUrl);
UPDATE magestore_megamenu_megamenu SET link = REPLACE(link, @oldAlternativeUrl, @newAlternativeUrl);

###DEFAULT QUERY TASCS
#removes all base urls except default when using dummy-sql with live data (invalid urls for test-environment)
DELETE FROM `core_config_data` WHERE `path` LIKE '%base_url%';

#set base urls for local env
REPLACE INTO core_config_data (scope, scope_id, path, value)
VALUES
    ('default', '0', 'web/unsecure/base_url', @newBaseUrl),
    ('default', '0', 'web/secure/base_url', @newBaseUrl),
    ('websites', '1', 'web/unsecure/base_url', @newBaseUrl),
    ('websites', '1', 'web/secure/base_url', @newBaseUrl);

#change inventory stock sales channel code to fix error
UPDATE `inventory_stock_sales_channel` SET `code` = 'base' WHERE `code` = 'mgo';

#In store_website update website_codes
UPDATE `store_website` SET `code` = 'mgo' WHERE `website_id` = '1';
UPDATE `store_website` SET `code` = 'base' WHERE `website_id` = '3';

####MGO SPECIFIC
#configure elastic
# REPLACE INTO `core_config_data` (`scope`, `scope_id`, `path`, `value`)
# VALUES
# ('default', 0, 'catalog/search/elasticsearch_server_hostname', 'elasticsearch'),
# ('default', 0, 'catalog/search/elasticsearch_server_port', '9200'),
# ('default', 0, 'catalog/search/elasticsearch_index_prefix', 'magento'),
# ('default', 0, 'catalog/search/elasticsearch_enable_auth', '1'),
# ('default', 0, 'catalog/search/elasticsearch_username', 'magento'),
# ('default', 0, 'catalog/search/elasticsearch_password', 'magento'),
# ('default', 0, 'catalog/search/elasticsearch7_server_timeout', '15'),
# ('default', 0, 'catalog/search/engine', 'elasticsearch7'),
# ('default', 0, 'catalog/search/elasticsearch7_password', 'magento'),
# ('default', 0, 'catalog/search/elasticsearch7_username', 'magento'),
# ('default', 0, 'catalog/search/elasticsearch7_enable_auth', '1'),
# ('default', 0, 'catalog/search/elasticsearch7_index_prefix', 'magento'),
# ('default', 0, 'catalog/search/elasticsearch7_server_port', '9200'),
# ('default', 0, 'catalog/search/elasticsearch7_server_hostname', 'elasticsearch');

#configure printformer (mgo_master)
DELETE FROM `core_config_data` WHERE `path` LIKE '%printformer/version2group%' AND `scope` = 'websites';

REPLACE INTO `core_config_data` (`scope`, `scope_id`, `path`, `value`)
VALUES
('default', 0, 'advanced/modules_disable_output/Mgo_Printformer', '0'),
('default', 0, 'advanced/modules_disable_output/Risscsolutions_Printformer', '0'),
('default', 0, 'advanced/modules_disable_output/Rissc_Printformer', '0'),
('default', 0, 'advanced/modules_disable_output/Rissc_PrintformerAdminAuth', '0'),
('default', 0, 'advanced/modules_disable_output/Rissc_PrintformerAuth', '0'),
('default', 0, 'printformer/color/attribute_enabled', '0'),
('default', 0, 'printformer/color/option_enabled', '0'),
('default', 0, 'printformer/color/option_values', '[]'),
('default', 0, 'printformer/color/query_parameter', 'risscw2pcustomcolorvariant'),
('default', 0, 'printformer/cron/cleanup_days', '30'),
('default', 0, 'printformer/cron/enabled', '0'),
('default', 0, 'printformer/format/attribute_enabled', '0'),
('default', 0, 'printformer/format/change_notice', '0'),
('default', 0, 'printformer/format/notice_text', 'Format change will reset your cofiguration progress.'),
('default', 0, 'printformer/format/option_enabled', '0'),
('default', 0, 'printformer/format/option_values', '[]'),
('default', 0, 'printformer/format/query_parameter', 'risscw2pcustomertemplate'),
('default', 0, 'printformer/general/allow_skip_config', '1'),
('default', 0, 'printformer/general/cart_edit_text', 'Edit'),
('default', 0, 'printformer/general/close_text', NULL),
('default', 0, 'printformer/general/config_button_css', NULL),
('default', 0, 'printformer/general/config_button_text', 'Online gestalten'),
('default', 0, 'printformer/general/delete_confirm_text', 'Should the draft get deleted?'),
('default', 0, 'printformer/general/delete_draft_button', '0'),
('default', 0, 'printformer/general/delete_feed_identifier', '0'),
('default', 0, 'printformer/general/display_mode', '2'),
('default', 0, 'printformer/general/draft_update', '0'),
('default', 0, 'printformer/general/enabled', '1'),
('default', 0, 'printformer/general/expire_date', '30'),
('default', 0, 'printformer/general/frame_fullscreen', '0'),
('default', 0, 'printformer/general/guest_wishlist_hint', 'You have to be registered to add this product to your Wishlist.'),
('default', 0, 'printformer/general/order_status', 'pending'),
('default', 0, 'printformer/general/printformer_upload_template_id', 'boVfihfH'),
('default', 0, 'printformer/general/processing_type', 'sync'),
('default', 0, 'printformer/general/product_image_preview', '0'),
('default', 0, 'printformer/general/redirect_after_config', '1'),
('default', 0, 'printformer/general/redirect_on_cancel', '1'),
('default', 0, 'printformer/version2group/v2apiKey', '7DdBOf2MEZQTEVyATIu1rv0iYrRw4vKL'),
('default', 0, 'printformer/version2group/v2identifier', 'DqLkAEsA'),
('default', 0, 'printformer/version2group/v2url', 'https://editor3.mgo-mediafactory.de'),
('default', 0, 'printformer/version2group/version2', '1'),
('default', 0, 'rissc_printformeradminauth/debug/enabled', '0'),
('default', 0, 'rissc_printformerauth/debug/enabled', '1');

# deactivate all order-export profiles
UPDATE `xtento_orderexport_profile` SET `enabled` = 0 where `enabled` != 0;

UPDATE `itoris_storelogincontrol_redirect` SET `custom_url` = @newBaseHomeUrl WHERE custom_url != @newBaseHomeUrl;

#set smtp-data mail.rissc.net
UPDATE `core_config_data` SET `value` = 'LOGIN' WHERE `path` = 'system/gmailsmtpapp/auth';
UPDATE `core_config_data` SET `value` = 'tls' WHERE `path` = 'system/gmailsmtpapp/ssl';
UPDATE `core_config_data` SET `value` = 'mail.rissc.net' WHERE `path` = 'system/gmailsmtpapp/smtphost';
UPDATE `core_config_data` SET `value` = 'rissc-ext/mgo-stage' WHERE `path` = 'system/gmailsmtpapp/username';
UPDATE `core_config_data` SET `value` = '1' WHERE `path` = 'system/gmailsmtpapp/set_reply_to';
UPDATE `core_config_data` SET `value` = '1' WHERE `path` = 'system/gmailsmtpapp/set_return_path';
UPDATE `core_config_data` SET `value` = 'ros@rissc.com' WHERE `path` = 'system/gmailsmtpapp/debug/email';
UPDATE `core_config_data` SET `value` = 'ros@mgo-mediafactory.de' WHERE `path` = 'system/gmailsmtpapp/debug/from_email';
UPDATE `core_config_data` SET `value` = '0' WHERE `path` = 'system/smtp/set_return_path';
UPDATE `core_config_data` SET `value` = '1' WHERE `path` = 'advanced/modules_disable_output/MagePal_GmailSmtpApp';
UPDATE `core_config_data` SET `value` = '587' WHERE `path` = 'system/gmailsmtpapp/smtpport';
UPDATE `core_config_data` SET `value` = 'localhost' WHERE `path` = 'system/gmailsmtpapp/name';
UPDATE `core_config_data` SET `value` = '0' WHERE `path` = 'system/gmailsmtpapp/set_from';
UPDATE `core_config_data` SET `value` = '1' WHERE `path` = 'system/gmailsmtpapp/active';

#set smtp-data sendinblue.com
# UPDATE `core_config_data` SET `value` = 'LOGIN' WHERE `path` = 'system/gmailsmtpapp/auth';
# UPDATE `core_config_data` SET `value` = 'none' WHERE `path` = 'system/gmailsmtpapp/ssl';
# UPDATE `core_config_data` SET `value` = 'smtp-relay.sendinblue.com' WHERE `path` = 'system/gmailsmtpapp/smtphost';
# UPDATE `core_config_data` SET `value` = 'ros@rissc.de' WHERE `path` = 'system/gmailsmtpapp/username';
# UPDATE `core_config_data` SET `value` = '0' WHERE `path` = 'system/gmailsmtpapp/set_reply_to';
# UPDATE `core_config_data` SET `value` = '0' WHERE `path` = 'system/gmailsmtpapp/set_return_path';
# UPDATE `core_config_data` SET `value` = 'ros@rissc.de' WHERE `path` = 'system/gmailsmtpapp/debug/email';
# UPDATE `core_config_data` SET `value` = 'ros@rissc.de' WHERE `path` = 'system/gmailsmtpapp/debug/from_email';
# UPDATE `core_config_data` SET `value` = '0' WHERE `path` = 'system/smtp/set_return_path';
# UPDATE `core_config_data` SET `value` = '0' WHERE `path` = 'advanced/modules_disable_output/MagePal_GmailSmtpApp';
# UPDATE `core_config_data` SET `value` = '587' WHERE `path` = 'system/gmailsmtpapp/smtpport';
# UPDATE `core_config_data` SET `value` = 'localhost' WHERE `path` = 'system/gmailsmtpapp/name';
# UPDATE `core_config_data` SET `value` = '0' WHERE `path` = 'system/gmailsmtpapp/set_from';
# UPDATE `core_config_data` SET `value` = '1' WHERE `path` = 'system/gmailsmtpapp/active';

#change default image adapter from imagemagick to gd2
UPDATE `core_config_data` SET `value` = 'GD2' WHERE `path` = 'dev/image/default_adapter' AND `scope_id` = '0';