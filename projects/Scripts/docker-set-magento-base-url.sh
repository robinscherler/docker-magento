#!/bin/sh
set -e

#import configs
. ./../Config/config.sh

# cd into project folder to execute commands
cd $PROJECT_PATH

dockergento exec bin/magento setup:store-config:set --base-url=$URL