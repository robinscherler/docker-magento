#!/bin/bash
set -o errexit

echo "Clearing the cache to apply updates..."
bin/clinotty bin/magento cache:flush