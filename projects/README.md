#Install new Project

### Step 1

cli step1

- git clone git@bitbucket.org:risscstuttgart/mgo.git mgo-test
- cd mgo-test
- git submodule init
- git submodule update

### Step 2

create this files with corresponding project configuration (use .samples with copy, paste and adjust)

- app/code/Rissc/Dockergento/Sources/Config/.env
- app/code/Rissc/Dockergento/Sources/Config/config.sh

[comment]: <>
(
    todo:
    example: 
    - app/code/Rissc/Dockergento/Sources/Querys/mgo/docker-import-customize.sql
    - app/code/Rissc/Dockergento/Sources/Querys/mgo/mgo-test/docker-import-customize.sql
    * logic adjustment for each environment like dev, prod, test
    * make sure to use correct env-variable in the yaml file
)

### Step 3

run command to sync required docker-compose files to main folder

> app/code/Rissc/Dockergento/Sources/Scripts/symlink-files-create.sh

### Step 4

run commands on project folder: (example)

- cd /Users/rsc/Sites/mgo-test

-   run one of two options:
    - option1
        - > dockergento start
    - option2
        - > cd app/code/Rissc/Dockergento/Sources/Scripts/
        - > deploy_docker.sh db-reset

- (check dbs, add entry to sequel)

### Step 5

check all project code references in app/code/Rissc/Dockergento, create and adjust files in repo like:

- app/code/Rissc/Dockergento/Sources/Querys/geiger
- app/code/Rissc/Dockergento/Sources/Config/Projects
- Volumes/Elements/Rissc/Docker/DB/Data/
- Volumes/Elements/Rissc/Docker

### Step 6

add entry to /etc/hosts for current env: (example)

- > sudo vi /etc/hosts
    - 127.0.0.1 mgo-test.loc

### Step 7

now you can run deploy-script

> cd app/code/Rissc/Dockergento/Sources/Scripts/
> deploy_docker.sh reset

### Step 8

search and save the rissc mail-server password in magento

####from:
https://mail.rissc.net/org/rissc-ext/servers/mgo-stage/credentials

####on f.e.
http://mgo-test.loc/controlcenter/admin/system_config/edit/section/system/

> you can test it by starting local cron-script:
> - 1: truncated cron_schedule-table
> - 2: run bin/magento setup:upgrade
> - 3: run app/code/Rissc/Dockergento/Sources/Scripts/./docker-cron.sh

# Start working with git submodules
- git submodule init

#### create submodule, commit the .gitmodules and the desired reference in the submodule location on the main branch

(like cloning, the folder will be created and should not exist before executing the command)

- git submodule add git@bitbucket.org:risscstuttgart/dockergento.git app/code/Rissc/Dockergento

#### fixing error
#### fatal: You are on a branch yet to be born, Unable to checkout submodule 'find-wrong-folder'
- rm -r .git/modules/find-wrong-folder

#### after the commit, you can always reload this commit of the submodule reference with following command:
- git submodule update
- git submodule update --remote

#### you can see any status changes here:
- git submodule status

#### to create symlinks of all files in the branch-folder "Sources"
- ln -s app/code/Rissc/Dockergento/Sources/* ./

#### how to remove submodules:
- Delete the relevant section from the .gitmodules file.
- Stage the .gitmodules changes git add .gitmodules
- Delete the relevant section from .git/config.
- Run git rm --cached path_to_submodule (no trailing slash).
- Run rm -rf .git/modules/path_to_submodule (no trailing slash).
- Commit git commit -m "Removed submodule "
- Delete the now untracked submodule files rm -rf path_to_submodule

# Test-configurations
## free smtp
https://my.sendinblue.com/

## ngrok with dockergento - using ngrok:
#### start ngrok
ngrok http 80

## configure printformer for ngrok:
#### on Verwaltung => Mandanten enter for mandant rissc
#### https://printformer.stage-00.aws.rissc.net/admin/clients/1
https://mgo-pp.stage-00.aws.rissc.net/rest/V1/printformer/api/acl

#### current test url changes every time we use ngrok
http://347fd1cf311e.ngrok.io/rest/V1/printformer/api/acl

## ngrok configuration:
#### save authtoken to ngrok-configuration
ngrok authtoken 1XKzwAxckaG48uAtZ3QkgamxcwE_7uo85CkYHENGzBdVmh4kn


## git - copy existing folder to submodule with history
Quellen: https://gist.github.com/korya/9047870
###### to check between steps, use: git log
###### example main repository git@bitbucket.org:risscstuttgart/geiger.git
###### example destination submodule repository git@bitbucket.org:risscstuttgart/rissc_configurableprintformer.git
###### example folder for submodule: app/code/Rissc/ConfigurablePrintformer

### to change master branch:
- git clone --no-hardlinks git@bitbucket.org:risscstuttgart/geiger.git temp
- cd temp
- git filter-branch --subdirectory-filter app/code/Rissc/ConfigurablePrintformer HEAD -- --all
- git reset --hard
- git gc --aggressive
- git prune
- git remote rm origin
- git remote add origin git@bitbucket.org:risscstuttgart/rissc_configurableprintformer.git
- git push origin master
- cd ..
- rm -rf temp

### to change any branch else (example dev):
- git clone --no-hardlinks git@bitbucket.org:risscstuttgart/geiger.git temp
- cd temp
- git checkout dev
- git fetch && git pull
- git filter-branch --subdirectory-filter app/code/Rissc/ConfigurablePrintformer HEAD -- --all
- git reset --hard
- git gc --aggressive
- git prune
- git remote rm origin
- git remote add origin git@bitbucket.org:risscstuttgart/rissc_configurableprintformer.git
- git push origin dev
- cd ..
- rm -rf temp

####ready! check bitbucket repo