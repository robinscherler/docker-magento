#!/bin/bash
set -o errexit

#help
#https://black.bird.eu/en/blog/how-to-use-ngrok-with-magento.html

#ngrok init command
#ngrok http 80

#ngrok example result:
#Forwarding   https://fb27-2003-e1-8721-3f7-c4e2-7b08-969d-9a69.ngrok.io -> http://localhost:80


#Set bin/magento as current folder position
CURRENT_NGROK="$(curl --silent --show-error http://localhost:4040/api/tunnels | sed -nE 's/.*public_url":"https:..([^"]*).*/\1/p')"
bin/setup-ssl "$CURRENT_NGROK"