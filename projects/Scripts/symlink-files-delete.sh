#!/bin/sh
set -e

#import configs
. ./../Config/config.sh

#remove all old symlinks from current folder
echo "symlinks to delete:"
find $PROJECT_PATH -maxdepth 1 -type l
find $PROJECT_PATH -maxdepth 1 -type l -delete
echo "finished"
