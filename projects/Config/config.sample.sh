#!/usr/bin/env bash
set -e

### adjust line 6 and line 8 with correct project and branch
#project
PROJECT_NAME='geiger'
#environment
PROJECT_ENV='dev'

PROJECT=$PROJECT_NAME'-'$PROJECT_ENV
PROJECT_PATH=/Users/rsc/Sites/magento/$PROJECT
DOM=loc
URL='http://'$PROJECT'.'$DOM'/'
DOCKERGENTO=$PROJECT_PATH/app/code/Rissc/Dockergento
SOURCES=$DOCKERGENTO/Sources
CONFIG=$SOURCES/Config
SYNC=$SOURCES/Sync

DB=$PROJECT
ACTION='update'
DB_HOST='db'
DB_USER='magento'
DB_PASSWORD='magento'
LANGUAGE='de_DE'
TIMEZONE='Europe/Berlin'
CURRENCY='EUR'
BACKEND_FRONTNAME='controlcenter'
ADMIN_USER=magento
ADMIN_PASSWORD=magento1234!
ADMIN_EMAIL=info+admin@rissc.de
ADMIN_FIRSTNAME=Magento
ADMIN_LASTNAME=Magento

#example dir for project source files path (.env.php .config.php, media.tar.gz)
IMPORT_PATH=./sources/$PROJECT_NAME/$PROJECT
ISSUES_PATH=./app/code/Rissc/Dockergento/Sources/Composer/sync
HOST_IMPORT_PATH=/Volumes/Elements/Rissc/Docker/Import/$PROJECT_NAME/$PROJECT