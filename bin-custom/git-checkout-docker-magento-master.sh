#!/bin/bash
set -o errexit

echo "Clearing the cache to apply updates..."
PWD=$(pwd)
cd submodules/docker-magento
git checkout master
git fetch && git pull
cd $PWD