#!/bin/bash
set -e

n=1

# continue until $n equals 5
while true; do
	bin/magento cron:run
	echo "Cron executed $n times."
	sleep 60
	n=$(( n+1 ))	 # increments $n
done
