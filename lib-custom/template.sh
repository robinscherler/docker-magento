#!/bin/bash

ls -la
cd rsc/submodule/docker-magento/
git checkout ${1}
cd ../../..
ls -la

##git checkout origin/master -- compose
cp -r rsc/submodule/docker-magento/compose/* ./
cp -r rsc/submodule/docker-magento/compose/.vscode ./