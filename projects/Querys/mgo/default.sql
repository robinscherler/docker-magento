###DEFAULT QUERY TASCS
#removes all base urls except default when using dummy-sql with live data (invalid urls for test-environment)
DELETE FROM `core_config_data` WHERE `path` LIKE '%base_url%';

#set base urls for local env
REPLACE INTO core_config_data (scope, scope_id, path, value)
VALUES
    ('default', '0', 'web/unsecure/base_url', 'http://mgo-dev.loc/'),
    ('default', '0', 'web/secure/base_url', 'http://mgo-dev.loc/'),
    ('websites', '3', 'web/unsecure/base_url', 'http://mgo-dev.loc/'),
    ('websites', '3', 'web/secure/base_url', 'http://mgo-dev.loc/');

#change inventory stock sales channel code to fix error
UPDATE `inventory_stock_sales_channel` SET `code` = 'base' WHERE `code` = 'mgo';

#In store_website update website_codes
UPDATE `store_website` SET `code` = 'mgo' WHERE `website_id` = '1';
UPDATE `store_website` SET `code` = 'base' WHERE `website_id` = '3';

#Update cms-container urls
UPDATE cms_block SET content = REPLACE(content, 'magento.vs12380.internet1.de', 'magento-stage.mgo-mediafactory.de');
UPDATE cms_block SET content = REPLACE(content, 'dennree.mgo-mediafactory.de', 'magento-stage.mgo-mediafactory.de');
UPDATE cms_page SET content = REPLACE(content, 'magento.vs12380.internet1.de', 'magento-stage.mgo-mediafactory.de');
UPDATE cms_page SET content = REPLACE(content, 'dennree.mgo-mediafactory.de', 'magento-stage.mgo-mediafactory.de');
UPDATE magestore_megamenu_megamenu SET link = REPLACE(link, 'magento.vs12380.internet1.de', 'magento-stage.mgo-mediafactory.de');
UPDATE magestore_megamenu_megamenu SET link = REPLACE(link, 'dennree.mgo-mediafactory.de', 'magento-stage.mgo-mediafactory.de');

####MGO SPECIFIC
#configure elastic
REPLACE INTO `core_config_data` (`scope`, `scope_id`, `path`, `value`)
VALUES
('default', 0, 'catalog/search/elasticsearch_server_hostname', 'elasticsearch'),
('default', 0, 'catalog/search/elasticsearch_server_port', '9200'),
('default', 0, 'catalog/search/elasticsearch_index_prefix', 'magento'),
('default', 0, 'catalog/search/elasticsearch_enable_auth', '1'),
('default', 0, 'catalog/search/elasticsearch_username', 'magento'),
('default', 0, 'catalog/search/elasticsearch_password', 'magento'),
('default', 0, 'catalog/search/elasticsearch7_server_timeout', '15'),
('default', 0, 'catalog/search/engine', 'elasticsearch7'),
('default', 0, 'catalog/search/elasticsearch7_password', 'magento'),
('default', 0, 'catalog/search/elasticsearch7_username', 'magento'),
('default', 0, 'catalog/search/elasticsearch7_enable_auth', '1'),
('default', 0, 'catalog/search/elasticsearch7_index_prefix', 'magento'),
('default', 0, 'catalog/search/elasticsearch7_server_port', '9200'),
('default', 0, 'catalog/search/elasticsearch7_server_hostname', 'elasticsearch');

#configure printformer (mgo_master)
DELETE FROM `core_config_data` WHERE `path` LIKE '%printformer/version2group%' AND `scope` = 'websites';
REPLACE INTO `core_config_data` (`scope`, `scope_id`, `path`, `value`)
VALUES
('websites', 3, 'printformer/version2group/version2', '1'),
('websites', 3, 'printformer/version2group/v2apiKey', 'ZK34H6YcnlwSPsuxzEshAm0xTMXCoThs'),
('websites', 3, 'printformer/version2group/v2identifier', 'LsnpHPj3'),
('websites', 3, 'printformer/version2group/v2url', 'https://editor3.mgo-mediafactory.de/');


#set smtp-data mail.rissc.net
UPDATE `core_config_data` SET `value` = 'LOGIN' WHERE `path` = 'system/gmailsmtpapp/auth';
UPDATE `core_config_data` SET `value` = 'tls' WHERE `path` = 'system/gmailsmtpapp/ssl';
UPDATE `core_config_data` SET `value` = 'mail.rissc.net' WHERE `path` = 'system/gmailsmtpapp/smtphost';
UPDATE `core_config_data` SET `value` = 'rissc-ext/mgo-stage' WHERE `path` = 'system/gmailsmtpapp/username';
UPDATE `core_config_data` SET `value` = '1' WHERE `path` = 'system/gmailsmtpapp/set_reply_to';
UPDATE `core_config_data` SET `value` = '1' WHERE `path` = 'system/gmailsmtpapp/set_return_path';
UPDATE `core_config_data` SET `value` = 'ros@rissc.com' WHERE `path` = 'system/gmailsmtpapp/debug/email';
UPDATE `core_config_data` SET `value` = 'ros@mgo-mediafactory.de' WHERE `path` = 'system/gmailsmtpapp/debug/from_email';
UPDATE `core_config_data` SET `value` = '0' WHERE `path` = 'system/smtp/set_return_path';
UPDATE `core_config_data` SET `value` = '1' WHERE `path` = 'advanced/modules_disable_output/MagePal_GmailSmtpApp';
UPDATE `core_config_data` SET `value` = '587' WHERE `path` = 'system/gmailsmtpapp/smtpport';
UPDATE `core_config_data` SET `value` = 'localhost' WHERE `path` = 'system/gmailsmtpapp/name';
UPDATE `core_config_data` SET `value` = '0' WHERE `path` = 'system/gmailsmtpapp/set_from';
UPDATE `core_config_data` SET `value` = '1' WHERE `path` = 'system/gmailsmtpapp/active';

#set smtp-data sendinblue.com
# UPDATE `core_config_data` SET `value` = 'LOGIN' WHERE `path` = 'system/gmailsmtpapp/auth';
# UPDATE `core_config_data` SET `value` = 'none' WHERE `path` = 'system/gmailsmtpapp/ssl';
# UPDATE `core_config_data` SET `value` = 'smtp-relay.sendinblue.com' WHERE `path` = 'system/gmailsmtpapp/smtphost';
# UPDATE `core_config_data` SET `value` = 'ros@rissc.de' WHERE `path` = 'system/gmailsmtpapp/username';
# UPDATE `core_config_data` SET `value` = '0' WHERE `path` = 'system/gmailsmtpapp/set_reply_to';
# UPDATE `core_config_data` SET `value` = '0' WHERE `path` = 'system/gmailsmtpapp/set_return_path';
# UPDATE `core_config_data` SET `value` = 'ros@rissc.de' WHERE `path` = 'system/gmailsmtpapp/debug/email';
# UPDATE `core_config_data` SET `value` = 'ros@rissc.de' WHERE `path` = 'system/gmailsmtpapp/debug/from_email';
# UPDATE `core_config_data` SET `value` = '0' WHERE `path` = 'system/smtp/set_return_path';
# UPDATE `core_config_data` SET `value` = '0' WHERE `path` = 'advanced/modules_disable_output/MagePal_GmailSmtpApp';
# UPDATE `core_config_data` SET `value` = '587' WHERE `path` = 'system/gmailsmtpapp/smtpport';
# UPDATE `core_config_data` SET `value` = 'localhost' WHERE `path` = 'system/gmailsmtpapp/name';
# UPDATE `core_config_data` SET `value` = '0' WHERE `path` = 'system/gmailsmtpapp/set_from';
# UPDATE `core_config_data` SET `value` = '1' WHERE `path` = 'system/gmailsmtpapp/active';
