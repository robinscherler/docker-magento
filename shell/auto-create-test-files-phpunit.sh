#!/bin/bash

# Check for uncommitted changes
for file in $(git diff --name-only); do
  # Check if it's a PHP class
  if [ "${file##*.}" == "php" ]; then
    # Create the tests directory if it doesn't exist
    Test_dir=$(git rev-parse --show-toplevel)/Test/Unit
    mkdir -p $Test_dir

    # Extract the class name and namespace from the file
    class_name=$(grep '^class' $file | awk '{print $2}')
    namespace=$(grep '^namespace' $file | awk -F ';' '{print $1}' | awk '{print $2}')

    path_variable="$namespace"
    # split the path into an array using '/' as the separator
    path_array=($(echo "$path_variable" | tr "\\" ' '))

    for ((i=0; i<2; i++))
    do
        for ((j=2; j<${#path_array[@]}; j++))
        do
          path_array[j]=${path_array[j+1]}
        done
        unset path_array[${#path_array[@]}-1]
    done

    # add "Test" to the third position of the array
    path_array[2]="Test"
    path_array[3]="Unit"

    # join the array back together into a string using '/' as the separator
    namespace_test=$(IFS="\\"; echo "${path_array[*]}")

    # Replace namespace separators with directory separators
    namespace_dir=$(echo $namespace | tr '\\' '/' | sed 's|^[^/]*/[^/]*/||')

    namespace_dir_formatted=${namespace_dir//\//\\}

    # Create the directory for the namespace if it doesn't exist
    namespace_tests_dir="$Test_dir/$namespace_dir"
    mkdir -p $namespace_tests_dir

    # Create the test file
    test_file="$namespace_tests_dir/${class_name}Test.php"

    full_formatted_dir="$namespace_test\\$namespace_dir_formatted"

    full_formatted_origin_dir="$namespace\\$class_name"
    if [ "${full_formatted_origin_dir: -1}" == "\\" ]; then
      full_formatted_origin_dir="${full_formatted_origin_dir%?}"
    fi

    echo "<?php

namespace $full_formatted_dir;

use $full_formatted_origin_dir;

class ${class_name}Test extends \\PHPUnit\\Framework\\TestCase {

    /** @var $class_name */
    private \$instance;

    public function setUp(): void
    {
    }

    public function testExample()
    {
        \$this->assertTrue(true);
    }
}" > $test_file
  fi
done
