#!/bin/bash
set -o errexit

echo "Clearing the cache to apply updates..."
PWD=$(pwd)
cd submodules/docker-magento
git checkout e9ff1a99
cd $PWD